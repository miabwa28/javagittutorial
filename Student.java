package com.remy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Student implements Comparable<Student> {
	
	private int rollno;  
	private String name;  
	private int age;  
	
	Student(int rollno,String name,int age){  
	this.rollno=rollno;  
	this.name=name;  
	this.age=age;  
	}  
	
	@Override
	public int compareTo(Student std) {
		// TODO Auto-generated method stub
		return new Integer(this.age).compareTo(new Integer(std.age));
	}
	
	public static void main(String[] args){
		
		ArrayList<Student> al=new ArrayList<Student>();  
		al.add(new Student(101,"Vijay",23));  
		al.add(new Student(106,"Ajay",27));  
		al.add(new Student(105,"Jai",21));  
		Comparator<Student> cmp=Collections.reverseOrder();
		Collections.sort(al,cmp);  
		for(Student st:al){  
		System.out.println(st.rollno+" "+st.name+" "+st.age);  
		}  
	      
	   }

	

}
